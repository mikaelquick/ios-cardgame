//
//  ViewController.swift
//  simpleCardGame
//
//  Created by Mikael  Quick on 2017-08-31.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Declaring
    var cardGame : CardGame!
    var timer : Timer!
    var effect : UIVisualEffect!
    var timerRunning : Bool!
    var countdown : Int = 15
    
    //Picture & Labels
    @IBOutlet weak var player1Card: UIImageView!
    @IBOutlet weak var player2Card: UIImageView!
    @IBOutlet weak var player1Score: UILabel!
    @IBOutlet weak var player2Score: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var winnerText: UILabel!
    @IBOutlet weak var statusMessage: UILabel!
    @IBOutlet weak var whoWon: UILabel!
    
    //Misc
    @IBOutlet var popUp: UIView!
    @IBOutlet var tapScreen: UITapGestureRecognizer!
    @IBOutlet weak var blur: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
        Assigning values. Had to assign the blur effect and then remove it since xcode was crying if I change3d the alpha to 0.
        */
        
        effect = blur.effect
        blur.effect = nil
        timerRunning = false
        popUp.layer.borderWidth = 2
        popUp.layer.borderColor = UIColor.black.cgColor
        popUp.layer.cornerRadius = 6
        
        cardGame = CardGame()
        cardGame.createDeck()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Tapgesture that cover the whole screen
    @IBAction func tapAnywhere(_ sender: Any) {
        
        //If the timer isnt running start the timer
        if(!timerRunning){
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(whileCountdown), userInfo: nil, repeats: true)
            timerRunning = true
      
        }
        
        cardGame.playRound(
            player1Image: player1Card,
            player2Image: player2Card,
            player1Score: player1Score,
            player2Score: player2Score,
            statusMessage: statusMessage,
            whoWon: whoWon)
    }
    
    //While timer is running
    func whileCountdown(){
        
        if(countdown == 0){
            
            UIView.animate(withDuration: 2.5) {
                
                self.blur.effect = self.effect
                self.popUp.alpha = 1
            }
            timer.invalidate()
            tapScreen.isEnabled = false
            
            winnerText.text = cardGame.whoWonMatch()
           self.view.addSubview(popUp)
            popUp.center = self.view.center
        }
        else{
            countdown -= 1
            timeLabel.text = String(countdown)
        }
    }
    
    //Reseting the values
    func reset(){
        UIView.animate(withDuration: 1) {
            self.blur.effect = nil
            self.popUp.alpha = 0
        }
        player1Card.image = UIImage(named: "back")
        player2Card.image = UIImage(named: "back")
        
        cardGame.p1Score = 0
        cardGame.p2Score = 0
        player1Score.text = "0"
        player2Score.text = "0"
        
        countdown = 15
        timerRunning = false
        timeLabel.text = "\(15)"
        statusMessage.text = "Press anywhere to play"
        tapScreen.isEnabled = true
        whoWon.text = " "
    }
    
    @IBAction func resetButton(_ sender: Any) {
        reset()
    }


}

