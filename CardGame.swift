//
//  cards.swift
//  cardGame
//
//  Created by Mikael  Quick on 2017-08-26.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import Foundation
import UIKit

class CardGame{
    
    //Init
    var value : Int = 0
    var picture : String!
    var mycards = [CardGame]()
    var p1Score = 0
    var p2Score = 0
    
    func createDeck(){
        
        /*Im a lazyman so I created this array to get all the pictures. Thereafter I just assigned values
         and pictures to the cards
        */
        var letters = ["c" , "d", "h", "s"]
        var valueOfCard = 2
        var cardNumber = 0
        
        for colors in 0...3{
            valueOfCard = 2
            for k in 1...13{
                
                mycards.append(CardGame())
                mycards[cardNumber].picture = ("\(letters[colors])\(k)")
                mycards[cardNumber].value = valueOfCard
                
                valueOfCard += 1
                cardNumber += 1
            }
        }
    }
    
    /*This function is called for playing each round. 
     I wanted to set the random values as var since they are changing, but xcode where crying.*/
    func playRound(player1Image : UIImageView ,
                  player2Image : UIImageView,
                  player1Score : UILabel,
                  player2Score : UILabel,
                  statusMessage : UILabel,
                  whoWon : UILabel){
        
        flipCard(imageViewToFlip: player1Image)
        flipCard(imageViewToFlip: player2Image)
        
        let random1 = Int(arc4random_uniform(52))
        let random2 = Int(arc4random_uniform(52))
        
        player1Image.image = UIImage(named: mycards[random1].picture)
        player2Image.image = UIImage(named: mycards[random2].picture)
        
        
        if(mycards[random1].value > mycards[random2].value){
            statusMessage.text = "Player1 Won"
            whoWon.text = ">"
            p1Score+=1
            player1Score.text = String(p1Score)
        }
            
        else if (mycards[random1].value < mycards[random2].value){
            statusMessage.text = "Player2 Won"
            whoWon.text = "<"
            p2Score+=1
            player2Score.text = String(p2Score)
            
        }
        else{
            whoWon.text = "="
            statusMessage.text = "It's a draw"
        }
    }
    
    //Since I had to use this more times I made it as a function
    func flipCard(imageViewToFlip : UIImageView){
        UIView.transition(with: imageViewToFlip, duration: 0.5, options:
            .transitionFlipFromLeft, animations: nil, completion: nil)
    }
    //Checking who is the beutiful winner
    func whoWonMatch() -> String{
        var winnerScore = 0
        
        if(p1Score > p2Score){
            winnerScore = p1Score - p2Score
            return "Player1 won with \(winnerScore)points"
        }
        else if(p2Score > p1Score){
            winnerScore = p2Score - p1Score
            return "Player2 won with \(winnerScore)points"
        }
        else{
            return "Draw"
        }
    }
}
